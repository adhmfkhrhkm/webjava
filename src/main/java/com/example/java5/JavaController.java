package com.example.java5;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class JavaController {

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Bootsy14.0";
    }

}